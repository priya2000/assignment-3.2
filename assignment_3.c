#include <stdio.h>
#include <stdlib.h>

int main()
{   int num;
    printf("Enter the number");
    scanf("%d",&num);

    // if number is perfectly divisible by 2
    if(num % 2 == 0)
        printf(" Even number.",num);
    else
        printf("Odd number.",num);
    return 0;
}
